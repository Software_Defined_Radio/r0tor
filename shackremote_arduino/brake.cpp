#include "brake.h"

Brake::Brake(int brake_pin, int release_delay_in_ms)
{
    last_release_ = 0;
    brake_counter_ = 0;
    brake_pin_ = brake_pin;
    brake_tightened_ = true;
    release_delay_in_ms_ = release_delay_in_ms;

    //Pin Settings
    pinMode(brake_pin_, OUTPUT);

    //All Relais Off
    digitalWrite(brake_pin_, HIGH);
}

void Brake::doBreak()
{
    if(brake_counter_ == 0)
    {
        if(brake_tightened_ == false)
        {
            int diff = millis() - last_release_;
            if((diff > release_delay_in_ms_) || (diff < 0))
            {
                digitalWrite(brake_pin_, HIGH);
                brake_tightened_ = true;
            }
        }
    }
    else
    {
        digitalWrite(brake_pin_, LOW);
        last_release_ = millis();
        brake_tightened_ = false;
    }
}

void Brake::skipDelay()
{
    last_release_ = millis() - release_delay_in_ms_ - 1;
}

void Brake::release()
{
    last_release_ = millis();
    brake_counter_++;
}

void Brake::tighten()
{
    brake_counter_--;
}
