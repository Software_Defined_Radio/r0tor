///
/// @file       settings.h
/// @copyright  Christian Obersteiner, Andreas Müller - CC-BY-SA 4.0
///
/// @brief  Contains all settings used by the rotor control
///

#ifndef SETTINGS_H
#define SETTINGS_H
// MISC Settings
#define DEBUG

//// Rotor Settings
// Rotor present
#define AZIM 1
#define ELEV 1

// Has Break?
#define AZIM_BREAK 1
#define ELEV_BREAK 1

// Allowed span in degree
#define AZIM_SPAN 1
#define ELEV_SPAN 1
// Max Values
#define AZIM_MIN 0.0
#define AZIM_MAX 360.0
#define ELEV_MIN 0.0
#define ELEV_MAX 180.0
// Resolution values in degree
#define AZIM_RES 1
#define ELEV_RES 1
// ADC Values
#define AZIM_ADC_MIN 0
#define AZIM_ADC_MAX 1024
#define ELEV_ADC_MIN 98
#define ELEV_ADC_MAX 1013
#define AZIM_DEG ((AZIM_ADC_MAX-AZIM_ADC_MIN)/(AZIM_MAX-AZIM_MIN))
#define ELEV_DEG ((ELEV_ADC_MAX-ELEV_ADC_MIN)/(ELEV_MAX-ELEV_MIN))

// Pin Settings
// Rotor Sensor Pins
#define AZIM_POT A1
#define ELEV_POT A0

// Rotor Relais
// Aximut
#define AZIM_RE_CW 49
#define AZIM_RE_CCW 47
#define AZIM_RE_BREAK 41
// Elevation
#define ELEV_RE_UP 43
#define ELEV_RE_DWN 45
#define ELEV_RE_BREAK 41

// Network Settings


////////////////////////////
// DONT TOUCH
////////////////////////////
// Typedefs
typedef unsigned int uint16;

#endif //SETTINGS_H
