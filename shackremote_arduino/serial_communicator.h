#ifndef SERIAL_COMMUNICATOR_H
#define SERIAL_COMMUNICATOR_H

#include "communicator.h"

#include "Arduino.h"

class SerialCommunicator: public Communicator
{
public:
    SerialCommunicator();
    String getCommandString(messageError*);
    messageError sendMessage(String);
private:
};

#endif // SERIAL_COMMUNICATOR_H
