///
/// @file       shackremote.ino
/// @copyright  Christian Obersteiner, Andreas Müller - CC-BY-SA 4.0
///
/// @brief  Contains all arduino stuff for the rotor control
///

#include <Arduino.h>
#include "settings.h"

#include <SPI.h>
#include <Ethernet.h>

#include "control.h"
#include "rotor.h"

// Network Settings
/// @todo move to settings.h
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x13, 0x37 };
IPAddress ip(83,133,178,126);
unsigned int localPort = 51337;

Rotor rotor;
EthernetUDP udp;
Control control(&rotor);
int debugTime;

void setup()
{


    // Open serial communications and wait for port to open:
    Serial.begin(19200);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }

    // start the Ethernet connection:
    //Ethernet.begin(mac, ip);
    //udp.begin(localPort);

    // init Hamlbib
    //hamlib.initHamlib(&udp, &rotor);

    //Debug
    debugTime = 0;

    //rotor.releaseBreakAzimuth();
    //rotor.releaseBreakElevation();
    //rotor.rotateUp();
    //control.setElevation(23);
    //control.setAzimuth(42);
}

void loop()
{
    //hamlib.receiveHamlibPacket();
    control.doControl();
    //hamlib.sendHamlibPacket();
}
