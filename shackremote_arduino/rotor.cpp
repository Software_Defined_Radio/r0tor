#include "rotor.h"

Rotor::Rotor()
{
    brake_ = new Brake(AZIM_RE_BREAK, 10000);
    azimuth_ = new Motor(AZIM_MAX, AZIM_MIN, AZIM_RE_CW, AZIM_RE_CCW, brake_, AZIM_POT, AZIM_DEG, AZIM_ADC_MIN, AZIM_RES, AZIM_SPAN);
    elevation_ = new Motor(ELEV_MAX, ELEV_MIN, ELEV_RE_DWN, ELEV_RE_UP, brake_, ELEV_POT, ELEV_DEG, ELEV_ADC_MIN, ELEV_RES, ELEV_SPAN);
}

void Rotor::doRotor()
{
    azimuth_->doMotor();
    elevation_->doMotor();
    brake_->doBreak();
}

void Rotor::stopAzimuth()
{
    azimuth_->stop();
}

void Rotor::stopElevation()
{
    elevation_->stop();
}

void Rotor::stopAndBreakAzimuth()
{
    stopAzimuth();
    brake_->skipDelay();
}

void Rotor::stopAndBreakElevation()
{
    stopElevation();
    brake_->skipDelay();
}

double Rotor::getAzimuth() const
{
    return azimuth_->getPosition();
}

double Rotor::getElevation() const
{
    return elevation_->getPosition();
}

Motor::motorError Rotor::setAzimuth(double pAzimuth)
{
    return azimuth_->setPosition(pAzimuth);
}

Motor::motorError Rotor::setElevation(double pElevation)
{
    return elevation_->setPosition(pElevation);
}
