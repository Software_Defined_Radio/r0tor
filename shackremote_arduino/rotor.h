///
/// @file       rotor.h
/// @copyright  Christian Obersteiner, Andreas Müller - CC-BY-SA 4.0
///
/// @brief  Contains all kind of rotor functions
///

#ifndef ROTOR_H
#define ROTOR_H

#include "settings.h"
#include "motor.h"
#include "brake.h"

class Rotor
{
public:
    Rotor();
    void doRotor();

    double getAzimuth() const;
    double getElevation() const;

    void stopAzimuth();
    void stopElevation();
    void stopAndBreakAzimuth();
    void stopAndBreakElevation();

    Motor::motorError setAzimuth(double);
    Motor::motorError setElevation(double);
private:
    Motor *azimuth_;
    Motor *elevation_;
    Brake *brake_;
};

#endif //ROTOR_H
