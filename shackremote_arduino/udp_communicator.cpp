#include "udp_communicator.h"

String UDPCommunicator::getCommandString(Communicator::messageError* msg_error)
{
  *msg_error = Communicator::MSG_NONE_AVAILABLE;
  return "";
}

Communicator::messageError UDPCommunicator::sendMessage(String message)
{
  return Communicator::MSG_NONE_AVAILABLE;
}
