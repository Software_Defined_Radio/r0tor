///
/// @file       hamlib.cpp
/// @copyright  Christian Obersteiner, Andreas Müller - CC-BY-SA 4.0
///
/// @brief  Contains all kind of hamlib functions
///

#include "hamlib.h"
#include <string.h>

///
/// @brief  Constructor
///

CHamlib::CHamlib()
{

}

///
/// @brief  Initialize Hamlib
///

void CHamlib::initHamlib(EthernetUDP *udp, Rotor *rotor)
{
    udp_ = udp;
    rotor_ = rotor;
}

///
/// @brief
///

void CHamlib::receiveHamlibPacket()
{
    if (packet_received_ == false) {
        int packet_size = udp_->parsePacket();
        if (packet_size) {
            packet_received_ = true;
            udp_->read(received_packet_, UDP_TX_PACKET_MAX_SIZE);
            parse_error_ = parseHamlibPacket();
        }
    }
}

///
/// @brief
///

void CHamlib::sendHamlibPacket()
{
    if (packet_parsed_) {
        udp_->beginPacket(udp_->remoteIP(), udp_->remotePort());
        udp_->write(tx_packet_);
        udp_->endPacket();
        packet_parsed_ = false;
    }
}

///
/// @brief
///

CHamlib::hamlibError CHamlib::parseHamlibPacket()
{
    packet_received_ = false;
    String token = strtok(received_packet_, ";");
    int dst = 0;

    while (token != NULL) {
        if ( token == "state" )
        {
            // Answer with max rotor values
            sprintf(tx_packet_, "%f/%f %f/%f", AZIM_MIN, AZIM_MAX, ELEV_MIN, ELEV_MAX);
            packet_parsed_ = true;
            sendHamlibPacket();
        }
        else if ( token == "get position" ) {
            sprintf(tx_packet_, "%f %f", rotor_->getAzimuth(), rotor_->getElevation());
            packet_parsed_ = true;
            sendHamlibPacket();
        }
        else if ( token.startsWith("set az") ) {
            if( token.length() < 8 )
                return HAMLIB_NOK;
            dst = token.substring(8).toInt();
            if ( dst == 0 && token.substring(8) != "0" )
                return HAMLIB_NOK;
            if ( dst < AZIM_MIN or dst > AZIM_MAX )
                return HAMLIB_NOK;
            rotor_->setAzimuth(dst);
        }
        else if ( token.startsWith("set ev") ) {
            if( token.length() < 8 )
                return HAMLIB_NOK;
            dst = token.substring(8).toInt();
            if ( dst == 0 && token.substring(8) != "0" )
                return HAMLIB_NOK;
            if ( dst < ELEV_MIN or dst > ELEV_MAX )
                return HAMLIB_NOK;
            rotor_->setElevation(dst);
        }
        else
            return HAMLIB_NOK;
    }

    return HAMLIB_OK;
}

///
/// @brief  Serial Debug output Function
///

void CHamlib::debugOut()
{
    Serial.print("Contents: ");
    Serial.println(received_packet_);
}
