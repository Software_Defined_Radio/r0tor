#include "serial_communicator.h"

SerialCommunicator::SerialCommunicator()
{
  Serial.setTimeout(100);
}

String SerialCommunicator::getCommandString(messageError *err)
{
  String result;

  if (Serial.available() > 0) {
    result = Serial.readString();
    *err = MSG_OK;
    return result;
  }

  *err = MSG_NONE_AVAILABLE;
  return "";
}

SerialCommunicator::messageError SerialCommunicator::sendMessage(String message)
{
  Serial.println(message);
  return Communicator::MSG_OK;
}
