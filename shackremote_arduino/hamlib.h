///
/// @file       hamlib.h
/// @copyright  Christian Obersteiner, Andreas Müller - CC-BY-SA 4.0
///
/// @brief  Contains all kind of hamlib functions
///

#ifndef HAMLIB_H
#define HAMLIB_H

#include <Arduino.h>
#include <Ethernet.h>

#include "settings.h"
#include "rotor.h"

class CHamlib
{
public:
    enum hamlibError{HAMLIB_OK,
                     HAMLIB_NOK };
    CHamlib();
    void initHamlib(EthernetUDP *udp, Rotor *rotor);
    void receiveHamlibPacket();
    void sendHamlibPacket();
    hamlibError parseHamlibPacket();
    void debugOut();

private:
    EthernetUDP *udp_;
    Rotor *rotor_;
    char received_packet_[UDP_TX_PACKET_MAX_SIZE];
    char tx_packet_[UDP_TX_PACKET_MAX_SIZE];
    bool packet_received_ = false;
    bool packet_parsed_ = false;
    hamlibError parse_error_ = HAMLIB_OK;
};

#endif //HAMLIB_H
