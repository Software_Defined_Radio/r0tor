#ifndef BRAKE_H
#define BRAKE_H

#include "settings.h"
#include "Arduino.h"

class Brake
{
public:
    Brake(int brake_pin, int release_delay_in_ms);
    void doBreak();

    void tighten();
    void release();
    void skipDelay();
private:
    int last_release_;
    int brake_counter_;
    int brake_pin_;
    int release_delay_in_ms_;
    byte brake_tightened_;
};

#endif //BRAKE_H
