#ifndef MOTOR_H
#define MOTOR_H

#include "Arduino.h"
#include "brake.h"

class Motor
{
public:
    enum motorError { ROT_OK,
                    ROT_NOK,
                    ROT_VAL_OUT_OF_RANGE,
                    ROT_BREAK_NOT_RELEASED,
                    ROT_ALREADY_IN_SPAN };
    Motor(double max, double min, int down_pin, int up_pin, Brake *brake, int sensor_pin, double degree, int adc_offset, double resolution, double span);
    void doMotor();
    void stop();
    double getPosition() const;

    motorError setPosition(double);
private:
    void releaseBrake();
    void tightenBrake();
    motorError rotateUp();
    motorError rotateDown();
    motorError readMotSensor();
    double readADC(int);

    Brake* brake_;
    double max_;
    double min_;
    int down_pin_;
    int up_pin_;
    int sensor_pin_;
    int adc_offset_;
    double position_;
    double destination_;
    double degree_;
    double resolution_;
    double span_;
    byte should_rotate_;
    byte brake_tightened_;
};

#endif //MOTOR_H
