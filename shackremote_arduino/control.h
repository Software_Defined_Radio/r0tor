#ifndef CONTROL_H
#define CONTROL_H

#include "Arduino.h"
#include "rotor.h"
#include "serial_communicator.h"
#include "udp_communicator.h"
#include "motor.h"

class Control
{
public:
    enum hamlibError{HAMLIB_OK,
                     HAMLIB_NOK };
    Control(Rotor *);
    void doControl();

    uint16 setAzimuth(uint16);
    uint16 setElevation(uint16);
    //uint16 getAz() const;
    //uint16 getEl() const;

private:
    void doCommunicate();
    hamlibError executeCommandString(String*, Communicator*);
    hamlibError executeActiveCommandSubstring(String);

    Rotor *rotor_;
    Communicator *serial_communicator_;
    Communicator *udp_communicator_;
    int debugTime_;
};

#endif // CONTROL_H
