#ifndef UDP_COMMUNICATOR_H
#define UDP_COMMUNICATOR_H

#include "communicator.h"

#include <Arduino.h>

class UDPCommunicator: public Communicator
{
public:
  String getCommandString(messageError*);
  messageError sendMessage(String);
private:
};

#endif // UDP_COMMUNICATOR_H
