#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <Arduino.h>

class Communicator
{
public:
    enum messageError { MSG_OK,
                    MSG_NONE_AVAILABLE };

    Communicator();
    virtual String getCommandString(messageError*);
    virtual messageError sendMessage(String);

private:
};

#endif // COMMUNICATOR_H
